package test.tut.com.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by John on 8/19/2015.
 */
public class CustomAdapter extends BaseAdapter {

    private static final int WIDTH = 250;
    private static final int HEIGHT = 250;
    private Context mContext;
    private String[] mImages;

    public CustomAdapter(Context mContext, String[] mImages) {
        this.mContext = mContext;
        this.mImages = mImages;
    }

    @Override
    public int getCount() {
        return mImages.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ImageView imageView;
        if (view == null) {
            imageView = new ImageView(mContext);
        } else {
            imageView = (ImageView)view;
        }
        Picasso.with(mContext)
                .load("file:///android_asset/" + mImages[i])
                .resize(200,200)
                .into(imageView);
        return imageView;
    }

    private void getImage(ImageView imageView, String image) {
        imageView.setImageBitmap(getBitmapFromAssets(image));
    }


    /**
     * get image from assets using asyntask
     * @param imageView
     * @param mImage
     */
    private void getImageUsingAsynTask(ImageView imageView, String mImage) {
        imageView.setTag(mImage);
        new LoadImageTask().execute(imageView);
    }

    /**
     * Helper class.
     */
    private class LoadImageTask extends AsyncTask<ImageView, Void, Bitmap> {

        ImageView imageView = null;

        @Override
        protected Bitmap doInBackground(ImageView... imageViews) {
            this.imageView = imageViews[0];

            return getBitmapFromAssets((String)imageView.getTag());
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            imageView.setImageBitmap(result);
        }
    }

    /**
     * read Bitmap from file using inputstream
     * @param file
     * @return
     */
    private Bitmap getBitmapFromAssets(String file) {
        InputStream is = null;
        Bitmap bitmap = null;
        AssetManager assetManager = mContext.getAssets();
        try {
            is = assetManager.open(file);
            bitmap = BitmapFactory.decodeStream(is);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }
}
