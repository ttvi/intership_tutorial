package test.tut.com.myapplication;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;


public class ThirdActivity extends ActionBarActivity {

    ImageView imageView;
    Button btnGetJSON;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        imageView = (ImageView) findViewById(R.id.imageView);
        btnGetJSON = (Button) findViewById(R.id.btn_get_json);

        Intent intent = getIntent();
        String fileName = intent.getStringExtra(SecondActivity.EXTRA_RES_IMAGE);
        imageView.setImageBitmap(getBitmapFromAssets(fileName));

        btnGetJSON.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new HttpGetTask().execute();
            }
        });
    }

    /**
     * read Bitmap from file using inputstream
     * @param file
     * @return
     */
    private Bitmap getBitmapFromAssets(String file) {
        InputStream is = null;
        Bitmap bitmap = null;
        AssetManager assetManager = getApplicationContext().getAssets();
        try {
            is = assetManager.open(file);
            bitmap = BitmapFactory.decodeStream(is);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    private class HttpGetTask extends AsyncTask<Void, Void, List<String>> {

        private static final String LIST = "list";
        private static final String URL = "http://104.238.152.185:5000/" + LIST;

        AndroidHttpClient mClient = AndroidHttpClient.newInstance("");

        @Override
        protected List<String> doInBackground(Void... params) {
            HttpGet request = new HttpGet(URL);
            JSONResponseHandler responseHandler = new JSONResponseHandler();
            try {
                return mClient.execute(request, responseHandler);
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<String> result) {
            if (mClient != null) {
                mClient.close();
            }
            String url = "http://104.238.152.185:5000/" + result.get(0).substring(7);
            Picasso.with(getApplicationContext()).load(url).into(imageView);
            super.onPostExecute(result);
        }
    }

    private class JSONResponseHandler implements ResponseHandler<List<String>> {


        private static final String DATA_TAG = "data";
        private static final String URL_TAG = "url";
        private static final String NAME_TAG = "name";

        @Override
        public List<String> handleResponse(HttpResponse httpResponse) throws ClientProtocolException, IOException {
            List<String> result = new ArrayList<String>();
            String JSONResponse = new BasicResponseHandler().handleResponse(httpResponse);
            try {
                JSONObject responObject = (JSONObject) new JSONTokener(JSONResponse).nextValue();
                JSONArray datas = responObject.getJSONArray(DATA_TAG);
                for (int idx = 0; idx < datas.length(); idx++) {
                    JSONObject data = (JSONObject) datas.get(idx);
                    result.add((String) data.get(URL_TAG));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return result;
        }
    }

    private Bitmap downloadImage(String url) {
        Bitmap bitmap = null;
        try {
            URL aUrl = new URL(url);
            URLConnection conn = aUrl.openConnection();
            conn.connect();
            InputStream is = conn.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            bitmap = BitmapFactory.decodeStream(bis);
            bis.close();
            is.close();
        } catch (Exception e) {
            Log.e("Download Task", "Error getting the image from server" + e.getMessage().toString());
        }
        return bitmap;
    }

}
