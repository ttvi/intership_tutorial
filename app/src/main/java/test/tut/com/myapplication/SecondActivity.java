package test.tut.com.myapplication;

import android.content.Intent;
import android.content.res.AssetManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import java.io.IOException;

public class SecondActivity extends ActionBarActivity {

    protected static final String EXTRA_RES_IMAGE = "IMAGE";
    // private String[] mImages;
    private AssetManager assetManager;
    GridView gridViewImages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        gridViewImages = (GridView) findViewById(R.id.gridView);

        assetManager = getAssets();

        /*try {
            int length = assetManager.list("").length;

            mImages  = new String[length];
            for (int i = 0; i < length; i++) {
                mImages[i] = assetManager.list("")[i];
            }
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        try {
            gridViewImages.setAdapter(new CustomAdapter(SecondActivity.this, assetManager.list("")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        gridViewImages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(SecondActivity.this, ThirdActivity.class);
                try {
                    intent.putExtra(EXTRA_RES_IMAGE, assetManager.list("")[(int)l]);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                startActivity(intent);
            }
        });
    }
}
